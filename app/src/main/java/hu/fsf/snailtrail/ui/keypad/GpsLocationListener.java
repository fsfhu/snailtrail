package hu.fsf.snailtrail.ui.keypad;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

public class GpsLocationListener implements LocationListener {

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(GpsLocationListener.class.getSimpleName(), "GPS status changed");
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        Log.i(GpsLocationListener.class.getSimpleName(), "GPS enabled");
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Log.i(GpsLocationListener.class.getSimpleName(), "GPS disabled");
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        final String latlon = location.getLatitude() + ", " + location.getLongitude();
        Log.i(GpsLocationListener.class.getSimpleName(), latlon);
    }

}
