package hu.fsf.snailtrail.ui.keypad;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.text.MessageFormat;
import java.util.Map;

import hu.fsf.snailtrail.R;
import hu.fsf.snailtrail.ui.settings.SettingsHelper;

import static android.content.Context.MODE_PRIVATE;

public class KeypadFragment extends Fragment {

    private boolean gpsTracked = false;

    private KeypadViewModel keypadViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Show the options
        super.setHasOptionsMenu(true);

        // Placeholder text
        this.keypadViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(KeypadViewModel.class);
        View root = inflater.inflate(R.layout.fragment_keypad, container, false);
        final TextView textView = root.findViewById(R.id.text_keypad);
        this.keypadViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.keypad, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_start_recording) {
            if (!gpsTracked) {
                startRecording();
            }
            return true;
        } else if  (id == R.id.action_freeze_gps) {
            Toast.makeText(super.getActivity(), R.string.action_freeze_gps, Toast.LENGTH_SHORT).show();
            return true;
        } else if  (id == R.id.action_take_photo) {
            Toast.makeText(super.getActivity(), R.string.action_take_photo, Toast.LENGTH_SHORT).show();
            return true;
        } else if  (id == R.id.action_take_voice_note) {
            Toast.makeText(super.getActivity(), R.string.action_take_voice_note, Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startRecording() {
        FragmentActivity activity = super.getActivity();

        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            final Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.dialog_title_location_required)
                    .setMessage(R.string.dialog_message_location_required)
                    .setPositiveButton(R.string.dialog_yes, (dialog, which) -> startActivity(settingsIntent))
                    .setNegativeButton(R.string.dialog_no, (dialog, which) -> {
                        // TODO: Notify user, that the app won't work
                    })
                    .show();
            return;
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Ask location permission
            final String[] permissions = { Manifest.permission.ACCESS_FINE_LOCATION };
            ActivityCompat.requestPermissions(activity, permissions, 0);
            // TODO: Notify user to click the button again
            return;
        }

        final LocationProvider locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
        final SettingsHelper settings = SettingsHelper.getInstance(activity);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, settings.getRefreshInterval(), settings.getMinimalDistance(), new GpsLocationListener());
        Toast.makeText(activity, R.string.event_recording_started, Toast.LENGTH_SHORT).show();
        this.gpsTracked = true;
    }

}