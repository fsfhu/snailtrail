package hu.fsf.snailtrail.ui.keypad;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class KeypadViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public KeypadViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is the keypad fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}