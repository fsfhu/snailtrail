package hu.fsf.snailtrail.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

import hu.fsf.snailtrail.R;

public class SettingsHelper {

    private final Context context;
    private final SharedPreferences preferences;

    private SettingsHelper(Context context) {
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SettingsHelper getInstance(Context context) {
        return new SettingsHelper(context);
    }

    private int getIntValue(int keyId, int defaultId) {
        final String value = preferences.getString(context.getString(keyId), context.getString(defaultId));
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            Log.w(SettingsHelper.class.getSimpleName(), "Invalid setting value", e);
            return Integer.parseInt(context.getString(defaultId));
        }
    }

    public int getRefreshInterval() {
        return this.getIntValue(R.string.setting_refresh_interval, R.string.setting_refresh_interval_default);
    }

    public int getMinimalDistance() {
        return this.getIntValue(R.string.setting_minimal_distance, R.string.setting_minimal_distance_default);
    }

}
